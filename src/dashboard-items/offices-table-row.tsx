import * as React from "react";

import { Office } from '../data/office';

export interface Props {
    office: Office;
}

export class OfficesTableRow extends React.Component<Props, {}> {
    private disabled: boolean;

    constructor() {
        super();
        this.disabled = false;
    }
    
    componentWillMount() {
    }

    render() {
        return (
            <tr>
                <td>{this.props.office.Address}</td>
                <td>{this.props.office.Address2}</td>
                <td>{this.props.office.City}</td>
                <td>{this.props.office.State}</td>
                <td>{this.props.office.Region}</td>
                <td>{this.props.office.Zip}</td>
                <td>{this.props.office.Lat}</td>
                <td>{this.props.office.Lng}</td>
                <td></td>
            </tr>
        );
    }
}
