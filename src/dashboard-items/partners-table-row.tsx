import * as React from "react";

import { Partner } from '../data/partner';
import { PartnerType } from '../data/partner.type';
import { User } from '../data/user';

export interface Props {
    partner: Partner;
    user: User;
    onUpdate: (event: any) => void;
    onSelected: (event: any) => void;
}

export interface States {
    partner: Partner;
}

export class PartnersTableRow extends React.Component<Props, States> {
    private disabled: boolean;

    constructor(partner: Partner) {
        super();
        this.disabled = false;
        this.state = { partner: new Partner() };
    }

    componentWillMount() {
        this.state.partner = this.props.partner;
    }

    isDisabled(item: Partner) {
        if (item.Approved !== undefined && item.Approved !== null) {
            return true;
        }
        return false;
    }

    toDirect(item: Partner) {
        if (!this.isDisabled(item)) {
            item.Type = PartnerType.Direct;
            item.Approved = new Date(Date.now());
            item.ApprovedBy = this.props.user.Id;
            item.Updated = new Date(Date.now());
            item.UpdatedBy = this.props.user.Id;
            item.AssignedTo = null;

            this.setState({ partner: item });
            this.props.onUpdate(this);
        }
    }

    toDistributor(item: Partner) {
        if (item.Type === PartnerType.Direct) {
            item.Type = PartnerType.Distributor;
            item.Updated = new Date(Date.now());
            item.UpdatedBy = this.props.user.Id;

            this.setState({ partner: item });
            this.props.onUpdate(this);
        }
    }

    onAssign(item: Partner) {
        if (!this.isDisabled(item)) {
            item.AssignedTo = this.props.user.Id;
            item.Updated = new Date(Date.now());
            item.UpdatedBy = this.props.user.Id;
            this.setState({ partner: item });
            console.log('Partner with Id:' + item.Id + ', was assigned to current user');

            this.props.onUpdate(this);
        }
    }

    render() {
        this.disabled = this.isDisabled(this.state.partner);

        return (
            <tr onClick={this.props.onSelected.bind(this, this.state.partner) }>
                <td>{this.state.partner.Id}</td>
                <td>{this.state.partner.Name}</td>
                <td>{PartnerType[this.state.partner.Type]}</td>
                <td>{(this.state.partner.Approved !== undefined && this.state.partner.Approved !== null) ? this.state.partner.Approved.toDateString() : ''}</td>
                <td>{this.state.partner.ApprovedBy}</td>
                <td>{this.state.partner.AssignedTo}</td>
                <td>
                    <a className="btn btn-xs btn-primary" disabled={this.disabled} onClick={this.toDirect.bind(this, this.state.partner) }>Make Direct</a>
                </td>
                <td>
                    <a className="btn btn-xs btn-primary" disabled={(this.state.partner.Type !== 1) } onClick={this.toDistributor.bind(this, this.state.partner) }>Make Distributor</a>
                </td>
                <td>
                    <a className="btn btn-xs btn-default" disabled={this.disabled} onClick={this.onAssign.bind(this, this.state.partner) }>Assign To Me</a>
                </td>
            </tr>
        );
    }
}
