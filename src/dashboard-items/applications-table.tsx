import * as React from "react";

import { Application } from '../data/application';
import { Partner } from '../data/partner';
import { PartnerType } from '../data/partner.type';
import { Office } from '../data/office';
import { User } from '../data/user';

import { ApplicationsTableRow } from './applications-table-row'
import { ApplicationsTableNew } from './applications-table-new'

export interface Props {
    applications: Array<Application>;
    partners: Array<Partner>;
    user: User;
    onApplicationsChange: (event: any) => void;
    onPartnersChange: (event: any) => void;
}

export class ApplicationsTable extends React.Component<Props, {}> {
    private columnSize: string;
    private wrapperStyle: any;

    private rows: any[];
    private newApplication: Application;

    constructor() {
        super();
        this.wrapperStyle = {
            maxWidht: '100%',
            overflowY: 'auto'
        }

        this.newApplication = new Application();
    }

    private handleRowUpdate(application: Application) {
        this.props.onApplicationsChange(this.props.applications);
    }

    private handleAddNewApplication(application: Application) {
        let nextId = this.props.applications.length + 1;
        application.Id = nextId;
        application.Created = new Date(Date.now());
        application.CreatedBy = this.props.user.Id;
        application.Received = new Date(Date.now());

        this.props.applications.push(application);
        this.props.onApplicationsChange(this.props.applications);
    }

    private createPartnerFromApplication(application: Application) {
        let nextId = this.props.partners.length + 1;
        
        let partner = new Partner();
        partner.Id = nextId;
        partner.Created = new Date(Date.now());
        partner.CreatedBy = application.CreatedBy;
        partner.Name = application.CompanyName;
        partner.Type = PartnerType.Indirect;

        let office = new Office();
        office.Address = application.Address;
        office.Address2 = application.Address2;
        office.City = application.City;
        office.Region = application.Region;
        office.State = application.State;
        office.Zip = application.Zip;
        office.Lat = application.Lat;
        office.Lng = application.Lng;
        partner.Offices.push(office);

        this.props.partners.push(partner);
        this.props.onPartnersChange(this.props.partners);
    }

    render() {
        this.rows = new Array<ApplicationsTableRow>();
        this.props.applications.forEach(
            application =>
                this.rows.push(
                    <ApplicationsTableRow
                        key={application.Id}
                        application={application}
                        user={this.props.user}
                        onUpdate={this.handleRowUpdate.bind(this, application)}
                        onAccept={this.createPartnerFromApplication.bind(this, application)}>
                    </ApplicationsTableRow>)
        );

        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Applications</h3>
                        </div>
                        <div className="panel-body">
                            <div style={this.wrapperStyle}>
                                <table className="table table-hover table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Address&nbsp;2</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Region</th>
                                            <th>Zip</th>
                                            <th>Latitude</th>
                                            <th>Longtitude</th>
                                            <th>Received</th>
                                            <th>Accepted</th>
                                            <th>Rejected</th>
                                            <th>Assigned&nbsp;To</th>
                                            <th colSpan="3"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.rows}
                                    </tbody>
                                    <tfoot>
                                        <ApplicationsTableNew
                                            onApplicationsChange={this.handleAddNewApplication.bind(this) }>
                                        </ApplicationsTableNew>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}