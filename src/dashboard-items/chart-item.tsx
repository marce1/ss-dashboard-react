import * as React from "react";
import * as ReactDom from "react-dom";
import * as Chart from "chart.js";

import { Application } from '../data/application';
import { Partner } from '../data/partner';

export interface Props {
    applications: Array<Application>;
    partners: Array<Partner>;
}
export interface States {
    chartApplicationsData: Array<number>;
    chartPartnersData: Array<number>;
}

export class ChartDashboardItem extends React.Component<Props, States> {
    private columnSize: string;
    private ctx: CanvasRenderingContext2D;
    private chart: Chart;

    private monthNames: Array<string> = [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
    ];
    public lineChartOptions: any = {
        responsive: true,
        responsiveAnimationDuration: 400,
        scales: {
            yAxes: [{
                gridLines: { color: 'rgba(207, 207, 207, 0.2)' },
                ticks: { suggestedMax: 4, stepSize: 1, fontColor: 'rgba(207, 207, 207, 0.8)' }
            }],
            xAxes: [{
                gridLines: { color: 'rgba(207, 207, 207, 0.2)' },
                ticks: { fontColor: 'rgba(207, 207, 207, 0.8)' }
            }]
        },
        options: {
            defaultFontColor: 'rgba(245, 245, 245, 1)'
        }
    };
    public linesSettings: Array<any> = [
        {
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,0.9)',
            pointBackgroundColor: 'rgba(148,159,177,0.9)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: 'rgba(101, 115, 139, 0.84)',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)',
            lineTension: 0,
            label: "Applications"
        },
        {
            backgroundColor: 'rgba(168, 104, 0, 0.53)',
            borderColor: 'rgba(239, 146, 31, 0.9)',
            pointBackgroundColor: 'rgba(239, 146, 31, 0.9)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: 'rgba(224, 135, 0, 1)',
            pointHoverBorderColor: 'rgba(136, 90, 37, 1)',
            lineTension: 0,
            label: "Partners"
        }
    ];

    constructor() {
        super();
        this.columnSize = 'col-sm-9';
        this.state = {
            chartApplicationsData: new Array<number>(1, 5),
            chartPartnersData: new Array<number>(5, 3, 5)
        }
    }

    refs: {
        [string: string]: any;
        chartCanvas: any;
    }

    lineData: Chart.LinearChartData = {
        labels: this.monthNames.slice(0, new Date(Date.now()).getMonth() + 1),
        datasets: this.linesSettings
    };

    componentDidMount() {
        let canvas: HTMLCanvasElement = ReactDom.findDOMNode(this.refs.chartCanvas) as HTMLCanvasElement;
        this.ctx = canvas.getContext('2d');

        this.updateApplicationsData(this.props.applications);
        this.updatePartnersData(this.props.partners);
        this.updateChart();
    }
    componentWillReceiveProps(nextProps: Props, nextContext: any) {
        this.updateApplicationsData(nextProps.applications);
        this.updatePartnersData(nextProps.partners);
        this.updateChart();
    }

    updateApplicationsData(applications: Array<Application>) {
        let data: Array<number> = new Array<number>(new Date(Date.now()).getMonth() + 1);
        data = Array.apply(null, Array(new Date(Date.now()).getMonth() + 1)).map(function () { return 0; });
        applications.forEach(x => data[x.Received.getMonth()]++);
        
        this.state.chartApplicationsData = data;
    }
    updatePartnersData(partners: Array<Partner>) {
        let data: Array<number> = new Array<number>(new Date(Date.now()).getMonth() + 1);
        data = Array.apply(null, Array(new Date(Date.now()).getMonth() + 1)).map(function () { return 0; });
        partners.forEach(x => data[x.Created.getMonth()]++);
        
        this.state.chartPartnersData = data;
    }

    updateChart() {
        this.lineData.datasets.filter(x => x.label === "Applications")[0].data = this.state.chartApplicationsData;
        this.lineData.datasets.filter(x => x.label === "Partners")[0].data = this.state.chartPartnersData;

        this.chart = new Chart(this.ctx, {
            type: "line",
            data: this.lineData,
            options: this.lineChartOptions
        });
    }

    render() {
        return (
            <div className={this.columnSize}>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <canvas ref="chartCanvas" />
                    </div>
                </div>
            </div>
        );
    }
}

class ChartData {
    data: Array<number>;
    label: string;
}