import * as React from "react";

import { Application } from '../data/application';
import { User } from '../data/user';

import { ApplicationsTableRow } from './applications-table-row'

export interface Props {
    onApplicationsChange: (event: any) => void;
}

export interface States {
    newApplication: Application;
}

export class ApplicationsTableNew extends React.Component<Props, States> {
    constructor() {
        super();
        this.state = { newApplication: new Application() }
    }

    private addApplication() {
        this.props.onApplicationsChange(this.state.newApplication);
        this.setState({ newApplication: new Application() });
    }

    render() {
        return (
            <tr>
                <td></td>
                <td>
                    <input
                        type="text"
                        placeholder="Company Name"
                        value={this.state.newApplication.CompanyName}
                        onChange={(event) => this.state.newApplication.CompanyName = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="Address"
                        value={this.state.newApplication.Address}
                        onChange={(event) => this.state.newApplication.Address = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="Address 2"
                        value={this.state.newApplication.Address2}
                        onChange={(event) => this.state.newApplication.Address2 = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="City"
                        value={this.state.newApplication.City}
                        onChange={(event) => this.state.newApplication.City = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="State"
                        value={this.state.newApplication.State}
                        onChange={(event) => this.state.newApplication.State = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="Region"
                        value={this.state.newApplication.Region}
                        onChange={(event) => this.state.newApplication.Region = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="Zip"
                        value={this.state.newApplication.Zip}
                        onChange={(event) => this.state.newApplication.Zip = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="number"
                        placeholder="Latitude"
                        step="any"
                        value={this.state.newApplication.Lat}
                        onChange={(event) => this.state.newApplication.Lat = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="number"
                        placeholder="Latitude"
                        step="any"
                        value={this.state.newApplication.Lng}
                        onChange={(event) => this.state.newApplication.Lng = (event.target as any).value} />
                </td>
                <td colSpan="7">
                    <a className="btn btn-xs btn-primary" onClick={this.addApplication.bind(this) }>Add Application</a>
                </td>
            </tr>
        );
    }
}