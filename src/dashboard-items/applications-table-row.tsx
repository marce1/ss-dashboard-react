import * as React from "react";

import { Application } from '../data/application';
import { User } from '../data/user';

export interface Props {
    application: Application;
    user: User;
    onUpdate: (event: any) => void;
    onAccept: (event: any) => void;
}

export interface States {
    application: Application;
}

export class ApplicationsTableRow extends React.Component<Props, States> {
    private disabled: boolean;

    constructor(application: Application) {
        super();
        this.disabled = false;
        this.state = { application: new Application() };
    }

    isDisabled(item: Application) {
        if (
            (item.Accepted !== undefined && item.Accepted !== null) ||
            (item.Declined !== undefined && item.Declined !== null)
        ) {
            return true;
        }
        return false;
    }

    onAccept(item: Application) {
        if (!this.isDisabled(item)) {
            item.Accepted = new Date(Date.now());
            item.Updated = item.Accepted;
            item.UpdatedBy = this.props.user.Id;
            item.AssignedTo = null;
            this.setState({ application: item });
            console.log('Application with Id:' + item.Id + ', was Acccepted');

            this.props.onUpdate(this);
            this.props.onAccept(this);
        }
    }
    onReject(item: Application) {
        if (!this.isDisabled(item)) {
            item.Declined = new Date(Date.now());
            item.Updated = item.Declined;
            item.UpdatedBy = this.props.user.Id;
            item.AssignedTo = null;
            this.setState({ application: item });
            console.log('Application with Id:' + item.Id + ', was Rejected');

            this.props.onUpdate(this);
        }
    }

    onAssign(item: Application) {
        if (!this.isDisabled(item)) {
            item.AssignedTo = this.props.user.Id;
            item.Updated = new Date(Date.now());
            item.UpdatedBy = this.props.user.Id;
            this.setState({ application: item });
            console.log('Application with Id:' + item.Id + ', was assigned to current user');

            this.props.onUpdate(this);
        }
    }

    render() {
        this.state.application = this.props.application;
        this.disabled = this.isDisabled(this.state.application);

        return (
            <tr>
                <td>{this.state.application.Id}</td>
                <td>{this.state.application.CompanyName}</td>
                <td>{this.state.application.Address}</td>
                <td>{this.state.application.Address2}</td>
                <td>{this.state.application.City}</td>
                <td>{this.state.application.State}</td>
                <td>{this.state.application.Region}</td>
                <td>{this.state.application.Zip}</td>
                <td>{this.state.application.Lat}</td>
                <td>{this.state.application.Lng}</td>
                <td>{(this.state.application.Received !== undefined && this.state.application.Received !== null) ? this.state.application.Received.toDateString() : ''}</td>
                <td>{(this.state.application.Accepted !== undefined && this.state.application.Accepted !== null) ? this.state.application.Accepted.toDateString() : ''}</td>
                <td>{(this.state.application.Declined !== undefined && this.state.application.Declined !== null) ? this.state.application.Declined.toDateString() : ''}</td>
                <td>{this.state.application.AssignedTo}</td>
                <td>
                    <a className="btn btn-xs btn-success" disabled={this.disabled} onClick={this.onAccept.bind(this, this.state.application) }>Accept</a>
                </td>
                <td>
                    <a className="btn btn-xs btn-danger" disabled={this.disabled} onClick={this.onReject.bind(this, this.state.application) }>Reject</a>
                </td>
                <td>
                    <a className="btn btn-xs btn-default" disabled={this.disabled} onClick={this.onAssign.bind(this, this.state.application) }>Assign To Me</a>
                </td>
            </tr>
        );
    }
}
