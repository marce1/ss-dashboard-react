import * as React from "react";

import { Application } from '../data/application';
import { Partner } from '../data/partner';
import { User } from '../data/user';

export interface Props {
    title: string;
    applications: Application[];
    parnters: Partner[];
    user: User;
}

export class UserActions extends React.Component<Props, {}> {
    private columnSize: string;
    private style: any;

    constructor() {
        super();
        this.columnSize = 'col-sm-3';
        this.style = {
            maxHeight: "430px"
        }
    }

    renderActions() {
        let applicationsCount = this.props.applications.filter(x => x.AssignedTo === this.props.user.Id).length;
        let partnersCount = this.props.parnters.filter(x => x.AssignedTo === this.props.user.Id).length;

        if (applicationsCount > 0 || partnersCount > 0) {
            return (
                <div className="panel-body user-actions">
                    <span id="applications-count" className="actions-count">{applicationsCount}</span>
                    <span id="partners-count" className="actions-count">{partnersCount}</span>
                </div>
            );
        }
        if (applicationsCount === 0 && partnersCount === 0) {
            return <span id="no-action" className="actions-count"><i className="fa fa-smile-o"></i></span>;
        }
    }

    render() {
        return (
            <div className={this.columnSize}>
                <div className="panel panel-info" style={this.style}>
                    <div className="panel-heading">
                        <h3 className="panel-title">{this.props.title}</h3>
                    </div>
                    <div className="panel-body user-actions">
                        { this.renderActions() }
                    </div>
                </div>
            </div>
        );
    }
}