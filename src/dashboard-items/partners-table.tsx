import * as React from "react";

import { Partner } from '../data/partner';
import { PartnerType } from '../data/partner.type';
import { Office } from '../data/office';
import { User } from '../data/user';

import { PartnersTableRow } from './partners-table-row';
import { PartnersTableNew } from './partners-table-new';
import { OfficesTableRow } from './offices-table-row';
import { OfficesTableNew } from './offices-table-new';

export interface Props {
    partners: Array<Partner>;
    user: User;
    onPartnersChange: (event: any) => void;
}

export interface States {
    selectedPartner: Partner;
}

export class PartnersTable extends React.Component<Props, States> {
    private wrapperStyle: any;

    private rows: Array<any>;
    private newPartner: Partner;

    private officeRows: Array<any>;
    private newOffice: Office;

    constructor() {
        super();
        this.wrapperStyle = {
            maxWidht: '100%',
            overflowY: 'auto'
        }
        this.newPartner = new Partner();
        this.state = { selectedPartner: null };
    }

    updateTable() {
        this.rows = new Array<PartnersTableRow>();
        this.props.partners.forEach(
            partner =>
                this.rows.push(
                    <PartnersTableRow
                        key={partner.Id}
                        partner={partner}
                        user={this.props.user}
                        onUpdate={this.handleRowUpdate.bind(this, partner) }
                        onSelected={this.handleSelect.bind(this, partner) } >
                    </PartnersTableRow>)
        );
    }

    private handleRowUpdate(partner: Partner) {
        this.props.onPartnersChange(this.props.partners);
    }
    private handleSelect(partner: Partner) {
        this.setState({ selectedPartner: partner });
        this.officeRows = new Array<OfficesTableRow>();
        partner.Offices.forEach(
            office =>
                this.officeRows.push(
                    <OfficesTableRow
                        key={office.Id}
                        office={office} >
                    </OfficesTableRow>)
        );
    }

    private handleAddNewPartner(partner: Partner) {
        let nextId = this.props.partners.length + 1;
        partner.Id = nextId;
        partner.Created = new Date(Date.now());
        partner.CreatedBy = this.props.user.Id;
        partner.Type = PartnerType.Indirect;

        this.props.partners.push(partner);

        this.props.onPartnersChange(this.props.partners);
    }
    private handleAddNewOffice(office: Office) {
        var partner = this.props.partners[this.props.partners.indexOf(this.state.selectedPartner)];
        office.Id = partner.Offices.length + 1;
        partner.Offices.push(office);

        this.props.onPartnersChange(this.props.partners);
        this.setState({ selectedPartner: partner });
        this.handleSelect(partner);
    }

    renderOffices() {
        if (this.state.selectedPartner !== undefined && this.state.selectedPartner !== null) {
            return (
                <div className="col-sm-12">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Offices of {this.state.selectedPartner.Name}</h3>
                        </div>
                        <div className="panel-body">
                            <div style={this.wrapperStyle}>
                                <table className="table table-hover table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Address</th>
                                            <th>Address&nbsp; 2</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Region</th>
                                            <th>Zip</th>
                                            <th>Latitude</th>
                                            <th>Longtitude</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.officeRows}
                                    </tbody>
                                    <tfoot>
                                        <OfficesTableNew
                                            onOfficesChange={this.handleAddNewOffice.bind(this) }>
                                        </OfficesTableNew>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }

    render() {
        this.updateTable();

        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">Partners</h3>
                        </div>
                        <div className="panel-body">
                            <div style={this.wrapperStyle}>
                                <table className="table table-hover table-condensed table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Approved</th>
                                            <th>Approved&nbsp;By</th>
                                            <th>Assigned&nbsp;To</th>
                                            <th colspan="3"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.rows}
                                    </tbody>
                                    <tfoot>
                                        <PartnersTableNew
                                            onPartnersChange={this.handleAddNewPartner.bind(this) }>
                                        </PartnersTableNew>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                { this.renderOffices() }
            </div>
        );
    }
}