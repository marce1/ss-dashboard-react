import * as React from "react";

import { Office } from '../data/office';

export interface Props {
    onOfficesChange: (event: any) => void;
}

export interface States {
    newOffice: Office;
}

export class OfficesTableNew extends React.Component<Props, States> {
    constructor() {
        super();
        this.state = { newOffice: new Office() }
    }

    private addOffice() {
        this.props.onOfficesChange(this.state.newOffice);
        this.setState({ newOffice: new Office() });
    }

    render() {
        return (
            <tr>
                <td>
                    <input
                        type="text"
                        placeholder="Address"
                        value={this.state.newOffice.Address}
                        onChange={(event) => this.state.newOffice.Address = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="Address 2"
                        value={this.state.newOffice.Address2}
                        onChange={(event) => this.state.newOffice.Address2 = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="City"
                        value={this.state.newOffice.City}
                        onChange={(event) => this.state.newOffice.City = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="State"
                        value={this.state.newOffice.State}
                        onChange={(event) => this.state.newOffice.State = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="Region"
                        value={this.state.newOffice.Region}
                        onChange={(event) => this.state.newOffice.Region = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="text"
                        placeholder="Zip"
                        value={this.state.newOffice.Zip}
                        onChange={(event) => this.state.newOffice.Zip = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="number"
                        placeholder="Latitude"
                        step="any"
                        value={this.state.newOffice.Lat}
                        onChange={(event) => this.state.newOffice.Lat = (event.target as any).value} />
                </td>
                <td>
                    <input
                        type="number"
                        placeholder="Latitude"
                        step="any"
                        value={this.state.newOffice.Lng}
                        onChange={(event) => this.state.newOffice.Lng = (event.target as any).value} />
                </td>
                <td>
                    <a className="btn btn-xs btn-primary" onClick={this.addOffice.bind(this)}>Add Office</a>
                </td>
            </tr>
        );
    }
}