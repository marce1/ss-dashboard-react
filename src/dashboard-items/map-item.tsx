// https://www.fullstackreact.com/articles/how-to-write-a-google-maps-react-component/
import * as React from "react";
import * as ReactDom from "react-dom";

import { Application } from '../data/application';
import { Partner } from '../data/partner';
import { Office } from '../data/office';

export interface Props {
    applications: Array<Application>;
    partners: Array<Partner>;
}
export interface States {
    markers: Array<google.maps.Marker>;
}

export class MapDashboardItem extends React.Component<Props, States> {
    private columnSize: string;
    private map: any;
    private mapStyle: any;
    private marker: google.maps.Marker;
    private mapOptions: google.maps.MapOptions;

    constructor() {
        super();
        this.columnSize = 'col-sm-12';
        this.mapStyle = {
            height: '300px'
        }

        let lat = 43.802715;
        let lng = -15.992815;
        this.mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 3
        }

        this.state = {
            markers: new Array<google.maps.Marker>()
        }
    }

    refs: {
        [string: string]: any;
        mapElement: any;
    }

    componentDidMount() {
        this.map = new google.maps.Map(ReactDom.findDOMNode(this.refs.mapElement), this.mapOptions);
        this.updateMarkers(this.props.applications, this.props.partners);
    }
    componentWillReceiveProps(nextProps: Props, nextContext: any) {
        this.updateMarkers(nextProps.applications, nextProps.partners);
    }

    updateMarkers(applications: Array<Application>, partners: Array<Partner>) {
        this.clearMap();
        applications = applications.filter(application =>
            (application.Accepted === undefined || application.Accepted === null) &&
            (application.Declined === undefined || application.Declined === null)
        );
        
        let applicationsToMarkers: Array<google.maps.Marker> = applications.map(function (application) {
            return new google.maps.Marker({
                position: new google.maps.LatLng(application.Lat, application.Lng),
                label: 'A',
                draggable: false
            });
        });
        let officesToMarkers = new Array<google.maps.Marker>();
        partners.forEach(partner => partner.Offices.map(function (office: Office) {
            officesToMarkers.push(new google.maps.Marker({
                position: new google.maps.LatLng(office.Lat, office.Lng),
                label: 'P',
                draggable: false
            }));
        }));

        applicationsToMarkers.forEach(marker => {
            marker.setMap(this.map);
            this.state.markers.push(marker)
        });
        officesToMarkers.forEach(marker => {
            marker.setMap(this.map);
            this.state.markers.push(marker);
        });
    }

    clearMap() {
        this.state.markers.forEach(marker => marker.setMap(null));
        this.state.markers = new Array<google.maps.Marker>();
    }

    render() {
        return (
            <div className={this.columnSize}>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <div style={this.mapStyle} ref="mapElement"></div>
                    </div>
                </div>
            </div>
        );
    }
}