import * as React from "react";

import { Partner } from '../data/partner';

export interface Props {
    onPartnersChange: (event: any) => void;
}

export interface States {
    newPartner: Partner;
}

export class PartnersTableNew extends React.Component<Props, States> {
    constructor() {
        super();
        this.state = { newPartner: new Partner() }
    }

    private addPartner() {
        this.props.onPartnersChange(this.state.newPartner);
        this.setState({ newPartner: new Partner() });
    }

    render() {
        return (
            <tr>
                <td></td>
                <td>
                    <input
                        type="text"
                        placeholder="Name"
                        value={this.state.newPartner.Name}
                        onChange={(event) => this.state.newPartner.Name = (event.target as any).value} />
                </td>
                <td colspan="7">
                    <a className="btn btn-xs btn-primary" onClick={this.addPartner.bind(this) }>Add Partner</a>
                </td>
            </tr>
        );
    }
}