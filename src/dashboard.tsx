import * as React from "react";

import { User } from './data/user';
import { Application } from './data/application';
import { APPLICATIONS } from './data/application.mock';
import { Partner } from './data/partner';
import { PARTNERS } from './data/partner.mock';

import { UserActions } from './dashboard-items/user-actions';
import { ApplicationsTable } from './dashboard-items/applications-table';
import { PartnersTable } from './dashboard-items/partners-table';
import { ChartDashboardItem } from './dashboard-items/chart-item';
import { MapDashboardItem } from './dashboard-items/map-item';

export interface Props {
    header: string;
    currentUser: User;
}
export interface States {
    applications: Application[];
    partners: Partner[];
}

export class Dashboard extends React.Component<Props, States> {
    constructor() {
        super();
        this.state = { applications: APPLICATIONS, partners: PARTNERS }
    }

    applicationsChanged(applications: Array<Application>) {
        this.setState({ applications: applications, partners: this.state.partners });
    }
    partnersChanged(partners: Array<Partner>) {
        this.setState({ applications: this.state.applications, partners: partners });
    }

    render() {
        return (
            <div>
                <h1>{this.props.header}</h1>
                <ul className="nav nav-pills">
                    <li className="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                    <li><a href="#partners" aria-controls="home" role="tab" data-toggle="tab">Partners</a></li>
                    <li><a href="#applications" aria-controls="home" role="tab" data-toggle="tab">Applications</a></li>
                </ul>

                <div className="tab-content">
                    <div role="tabpanel" className="tab-pane active" id="home">
                        <div className="row">
                            <UserActions user={this.props.currentUser} title='Your actions' applications={this.state.applications} parnters={this.state.partners}></UserActions>
                            <ChartDashboardItem applications={this.state.applications} partners={this.state.partners}/>
                        </div>
                        <div className="row">
                            <MapDashboardItem applications={this.state.applications} partners={this.state.partners}/>
                        </div>
                    </div>
                    <div role="tabpanel" className="tab-pane" id="partners">
                        <PartnersTable user={this.props.currentUser} partners={this.state.partners} onPartnersChange={this.partnersChanged.bind(this)}></PartnersTable>
                    </div>
                    <div role="tabpanel" className="tab-pane" id="applications">
                        <ApplicationsTable 
                        user={this.props.currentUser} 
                        applications={this.state.applications}
                        onApplicationsChange={this.applicationsChanged.bind(this)}
                        partners={this.state.partners} 
                        onPartnersChange={this.partnersChanged.bind(this)}></ApplicationsTable>
                    </div>
                </div>
            </div>
        );
    }
}