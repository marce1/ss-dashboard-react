import * as React from "react";

import { Navigation } from './navigation'
import { Dashboard } from './dashboard'

import { User } from './data/user'
import { USERS } from './data/user.mock';

export class App extends React.Component<{}, {}> {
    private user: User;
    private s: string;

    constructor() {
        super();
        this.user = USERS[0];
    }

    render() {
        return (
            <div>
                <Navigation currentUser={this.user}></Navigation>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <Dashboard header="Dashboard" currentUser={this.user}></Dashboard></div>
                    </div>
                </div>
            </div>
        );
    }
}