import * as React from "react";

import { User } from './data/user';

export interface Props { currentUser: User }

export class Navigation extends React.Component<Props, {}> {
    render() {
        return (
            <nav className="navbar navbar-inverse" role="navigation">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="#"><img src='../content/logo.png' className="logo-image"/></a>
                    </div>

                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <a>{this.props.currentUser.FirstName + ' ' + this.props.currentUser.LastName}</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}