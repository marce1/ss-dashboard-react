import { User } from './user';

let user = new User();
user.Id = 1;
user.Created = new Date(2016, 5, 29);
user.FirstName = 'Marcel';
user.LastName = 'Granak';

export var USERS: User[] = [user];