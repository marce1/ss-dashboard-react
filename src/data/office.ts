import {Data} from './data';

export class Office extends Data {
    public Address: string;
    public Address2: string;
    public City: string;
    public State: string;
    public Region: string;
    public Zip: string;
    public Lat: number;
    public Lng: number;
}
