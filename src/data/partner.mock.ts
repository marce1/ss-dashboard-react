import { Partner } from './partner';

import { PartnerType } from './partner.type';
import { Office } from './office';

let divali = new Partner();
let divaliOffices = new Array<Office>();
let officeSrbia = new Office();
officeSrbia.Id = 1;
officeSrbia.Address = '200 JURIJA GAGARINA';
officeSrbia.City = 'Beograd';
officeSrbia.State = 'Srbia';
officeSrbia.Lat = 44.8024051;
officeSrbia.Lng = 20.3879715;
divaliOffices.push(officeSrbia);

divali.Id = 1;
divali.Name = 'Divali Design';
divali.Created = new Date(2015, 2, 12);
divali.CreatedBy = 1;
divali.Type = PartnerType.Indirect;
divali.Offices = divaliOffices;
divali.AssignedTo = 1;

let usNetwork = new Partner();
let usNetworkOffices = new Array<Office>();
let officeRichfield = new Office();
officeRichfield.Id = 2;
officeRichfield.Address = '3554 Brecksville Rd';
officeRichfield.Address2 = '#200';
officeRichfield.City = 'Richfield';
officeRichfield.Region = 'OH';
officeRichfield.State = 'United States';
officeRichfield.Zip = '44286';
officeRichfield.Lat = 41.2299043;
officeRichfield.Lng = -81.6401127;
usNetworkOffices.push(officeRichfield);

usNetwork.Id = 2;
usNetwork.Name = 'US Network Inc';
usNetwork.Created = new Date(2015, 3, 5);
usNetwork.CreatedBy = 1;
usNetwork.Approved = new Date(2015, 3, 22);
usNetwork.ApprovedBy = 1;
usNetwork.Type = PartnerType.Direct;
usNetwork.Offices = usNetworkOffices;

export var PARTNERS: Partner[] = [divali, usNetwork];
