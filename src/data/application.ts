import { Data } from './data';

export class Application extends Data{
    public CompanyName: string;
    public Address: string;
    public Address2: string;
    public City: string;
    public State: string;
    public Region: string;
    public Zip: string;
    public Received: Date;
    public Accepted: Date;
    public Declined: Date;
    public AssignedTo: number;
    public Lat: number;
    public Lng: number;
}
