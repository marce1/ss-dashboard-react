import { Data } from './data';

import { PartnerType } from './partner.type';
import { Office } from './office';

export class Partner extends Data {
    public Name: string;
    // Approved - as Direct partner
    public Approved: Date;
    public ApprovedBy: number;
    public Type: PartnerType;
    public Offices: Array<Office>;
    public AssignedTo: number;
    constructor(){
        super();
        this.Offices = new Array<Office>();
    }
}
