export class Data {
    public Id: number;
    public Created: Date;
    public CreatedBy: number;
    public Updated: Date;
    public UpdatedBy: number;
}